library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor_5 is
	generic(
		SA1 :positive:=475;--size of adder 1
		SR  :positive:=171;--size of root
		SI	 :positive:=512;--size of input data
		AB  :positive:=1   --adjusting bit
	);
	port( 
		CLK																												: in  std_logic;	
		load_reg_D, load_reg_S, load_reg_W, load_reg_R, clr_reg_Q, shift_reg_Q					   : in  std_logic;
		Data																												: in  std_logic_vector(SI/2-1 downto 0);
		Sel_D																												: in	std_logic;
		OUTPUT																											: out std_logic_vector(SR-1 downto 0)
		--REMAINDER																										: out	std_logic_vector(22 downto 0);
	);
end entity main_processor_5;

architecture RTL of main_processor_5 is
	signal Sel_B			:std_logic_vector(1  downto 0);
	signal A1,B1,S1		:std_logic_vector(SA1-1 downto 0);
	signal A2,B2,S2		:std_logic_vector(SA1+2 downto 0);
	signal A3,B3,S3		:std_logic_vector(SA1+2 downto 0);
	signal A4,B4,S4		:std_logic_vector(SA1+2 downto 0);
	signal A5,B5,S5		:std_logic_vector(SA1+2 downto 0);
	signal A6,B6,S6		:std_logic_vector(SA1-1 downto 0);
	signal A7,B7,S7		:std_logic_vector(SA1-1 downto 0);
	signal A8,B8,S8		:std_logic_vector(SA1-1 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(SI+AB-4 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(SA1-1 downto 0);
	signal INPUT_R, Q_R	:std_logic_vector(SA1-1 downto 0);
	signal INPUT_W, Q_W	:std_logic_vector(SA1+2 downto 0);
	signal Q_Q				:std_logic_vector(SR-1 downto 0);
	signal Dato 			:std_logic_vector(SI+AB-1 downto 0):=(others=>'0');


	begin
	
	Dato(SI/2-1 downto 0)<=Data;
	
	A1<=Q_S;
	B1(SR-1 downto 0)<=Q_Q;
	B1(SA1-1 downto SR)<=(others=>'0');
	
	A2<="000"&Q_S;
	B2<="000"&Q_S;
	
	A3<=S4;
	B3<=S5;
	
	A4<="000"&Q_S;
	B4(SR-1 downto 0)<=Q_Q;
	B4(SA1+2 downto SR)<=(others=>'0');

	
	A5<="000"&Q_S;
	B5(SR-1 downto 0)<=Q_Q;
	B5(SA1+2 downto SR)<=(others=>'0');

	
	A6<=Q_R;
	B6<=Q_W(SA1-1 downto 0);
	
	A7<=S6;
	B7<=S8;
	
	A8(SR-1 downto 0)<=Q_Q;
	A8(SA1-1 downto SR)<=(others=>'0');
	B8(SR-1 downto 0)<=Q_Q;
	B8(SA1-1 downto SR)<=(others=>'0');

		
	with S7(SA1-1) select INPUT_W<=
		S2 when '0',
		S3 when '1';
		
	with S7(SA1-1) select INPUT_S<=
		Q_S when '0',
		S1	 when '1';
		
	with S7(SA1-1) select INPUT_R(SA1-1 downto 3)<=
		Q_R(SA1-4 downto 0)	when '0',
		S7(SA1-4 downto 0) 					when '1';
		
	with Sel_D select INPUT_R(2 downto 0)<=
		Q_D(SI+AB-4 downto SI+AB-6) when '0',
		Dato(SI+AB-1 downto SI+AB-3) when '1';
		
	with Sel_D select INPUT_D<=
		Q_D(SI+AB-7 downto 0)&"000" when '0',
		Dato(SI+AB-4 downto 0) when '1';
		
	REG_w: lpm_ff
		generic map(
			lpm_width	=>SA1+3
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_w,
			data	=>INPUT_w,
			q		=>Q_w
		);	
		
	REG_D: lpm_ff
		generic map(
			lpm_width	=>SI+AB-3
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	


	REG_S: lpm_ff
		generic map(
			lpm_width	=>SA1
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_Q: lpm_shiftreg
		generic map(
			lpm_width => SR
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_Q,
			shiftin	=> (not S7(SA1-1)),
			enable	=> shift_reg_Q,
			q			=> Q_Q
		);

	
	REG_R: lpm_ff
		generic map(
			lpm_width  =>SA1
		)
		port map(
			clock	=>CLK,
			sload =>load_reg_R,
			data	=>INPUT_R,
			q		=>Q_R
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>SA1,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A1,
			datab		=>B1,
			result	=>S1			
		);
	
	ALU2: lpm_add_sub
		generic map(
			lpm_width				=>SA1+3,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A2,
			datab		=>B2,
			result	=>S2		
		);
	
	ALU3: lpm_add_sub
		generic map(
			lpm_width				=>SA1+3,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A3,
			datab		=>B3,
			result	=>S3		
		);
		
	ALU4: lpm_add_sub
		generic map(
			lpm_width				=>SA1+3,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A4,
			datab		=>B4,
			result	=>S4		
		);	
	
	ALU5: lpm_add_sub
		generic map(
			lpm_width				=>SA1+3,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A5,
			datab		=>B5,
			result	=>S5		
		);
	
	ALU6: lpm_add_sub
		generic map(
			lpm_width				=>SA1,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A6,
			datab		=>B6,
			result	=>S6		
		);
	
	ALU7: lpm_add_sub
		generic map(
			lpm_width				=>SA1,
			lpm_representation	=>"unsigned"
		)
		port map(
			dataa		=>A7,
			datab		=>B7,
			add_sub	=>'0',
			result	=>S7		
		);
	
	ALU8: lpm_add_sub
		generic map(
			lpm_width				=>SA1,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A8,
			datab		=>B8,
			result	=>S8		
		);	

	OUTPUT	 <=	Q_Q;
	--REMAINDER <=	Q_S;
		
end RTL;