library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor_param is
	generic(
		N :positive:=86;
		B :positive:=2
	);
	port( 
		CLK														: in  std_logic;	
		load_reg_D, load_reg_S, load_reg_F			   : in  std_logic;
		set_reg_F, clr_reg_R, shift_reg_R, Sel_D		: in	std_logic;
		Dato														: in  std_logic_vector(3*N-B-1 downto 0);
		--REMAIN													: out	std_logic_vector(3*N-B-1 downto 0);
		--C														: out std_logic;
		OUTPUT													: out std_logic_vector(N-1 downto 0)
	);
end entity main_processor_param;

architecture RTL of main_processor_param is
	signal A1,B1,S1,M1	:std_logic_vector(3*N-B-1 downto 0);
	signal A2,B2,S2		:std_logic_vector(3*N-B-1 downto 0);
	signal A3,B3,S3		:std_logic_vector(3*N-B-1 downto 0);
	signal CO,EN_F			:std_logic;
	signal Q_R				:std_logic_vector(N-1 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(3*N-1 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(3*N-B-1 downto 0);
	signal INPUT_F, Q_F	:std_logic_vector(3*N-B-1 downto 0);
	signal padding			:std_logic_vector(B-1  downto 0):=(others=>'0');

	begin
		
	with Sel_D select INPUT_D<=	
		padding&Dato				when '1',
		Q_D(3*N-4 downto 0)&"000"	when '0';
	
	A2<=Q_F(3*N-B-3 downto 1)&"001";
	
	B2(N+1 downto 0)<=Q_R&CO&'0';
	B2(3*N-B-1 downto N+2)<=(others=>'0');
	
	A3<=S2;
	
	with CO select B3(N+4 downto 0)<=
		'0'&Q_R&"0000"	when '0',
		Q_R&"10000"		when '1';
	
	B3(3*N-B-1 downto N+5)<=(others=>'0');
	
	with CO select M1<=
		Q_S(3*N-B-4 downto 0)&Q_D(3*N-1 downto 3*N-3) when '0',
		S1											  when '1';
	
	REG_D: lpm_ff
		generic map(
			lpm_width	=>3*N
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			enable=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	

	REG_S: lpm_ff
		generic map(
			lpm_width	=>3*N-B
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			enable=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_R: lpm_shiftreg
		generic map(
			lpm_width => N
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_R,
			shiftin	=> CO,
			enable	=> shift_reg_R,
			q			=> Q_R
		);

	EN_F <= set_reg_F or load_reg_F;
	
	REG_F: lpm_ff
		generic map(
			lpm_width  =>3*N-B,
			lpm_svalue => "1"
		)
		port map(
			clock	=>CLK,
			enable=>EN_F,
			sload =>load_reg_F,
			sset  =>set_reg_F,
			data	=>INPUT_F,
			q		=>Q_F
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>3*N-B,
			lpm_direction			=>"SUB",
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A1,
			datab		=>B1,
			result	=>S1,			
			cout		=>CO
		);
	
	ALU2: lpm_add_sub
		generic map(
			lpm_width				=>3*N-B,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A2,
			datab		=>B2,
			result	=>S2		
		);
	
	ALU3: lpm_add_sub
		generic map(
			lpm_width				=>3*N-B,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A3,
			datab		=>B3,
			add_sub	=>CO,
			result	=>S3		
		);
	
	A1<=Q_S(3*N-B-4 downto 0)&Q_D(3*N-1 downto 3*N-3);
	B1<=Q_F;
	
	INPUT_S<=M1;
	INPUT_F<=S3;
		
	OUTPUT	<=	Q_R;
	--REMAIN	<= Q_S;
	--C			 <= 	not CO;
	--FACTOR <=	Q_F;
		
end RTL;