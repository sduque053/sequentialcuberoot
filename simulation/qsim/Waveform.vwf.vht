-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "07/13/2020 16:39:38"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          main_processor_param
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY main_processor_param_vhd_vec_tst IS
END main_processor_param_vhd_vec_tst;
ARCHITECTURE main_processor_param_arch OF main_processor_param_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK : STD_LOGIC;
SIGNAL clr_reg_R : STD_LOGIC;
SIGNAL FACTOR : STD_LOGIC_VECTOR(470 DOWNTO 0);
SIGNAL load_reg_D : STD_LOGIC;
SIGNAL load_reg_F : STD_LOGIC;
SIGNAL load_reg_S : STD_LOGIC;
SIGNAL Sel_D : STD_LOGIC;
SIGNAL set_reg_F : STD_LOGIC;
SIGNAL shift_reg_R : STD_LOGIC;
COMPONENT main_processor_param
	PORT (
	CLK : IN STD_LOGIC;
	clr_reg_R : IN STD_LOGIC;
	FACTOR : OUT STD_LOGIC_VECTOR(470 DOWNTO 0);
	load_reg_D : IN STD_LOGIC;
	load_reg_F : IN STD_LOGIC;
	load_reg_S : IN STD_LOGIC;
	Sel_D : IN STD_LOGIC;
	set_reg_F : IN STD_LOGIC;
	shift_reg_R : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : main_processor_param
	PORT MAP (
-- list connections between master ports and signals
	CLK => CLK,
	clr_reg_R => clr_reg_R,
	FACTOR => FACTOR,
	load_reg_D => load_reg_D,
	load_reg_F => load_reg_F,
	load_reg_S => load_reg_S,
	Sel_D => Sel_D,
	set_reg_F => set_reg_F,
	shift_reg_R => shift_reg_R
	);

-- CLK
t_prcs_CLK: PROCESS
BEGIN
LOOP
	CLK <= '0';
	WAIT FOR 50000 ps;
	CLK <= '1';
	WAIT FOR 50000 ps;
	IF (NOW >= 17200000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_CLK;

-- clr_reg_R
t_prcs_clr_reg_R: PROCESS
BEGIN
	clr_reg_R <= '1';
	WAIT FOR 100000 ps;
	clr_reg_R <= '0';
WAIT;
END PROCESS t_prcs_clr_reg_R;

-- load_reg_D
t_prcs_load_reg_D: PROCESS
BEGIN
	load_reg_D <= '1';
WAIT;
END PROCESS t_prcs_load_reg_D;

-- load_reg_F
t_prcs_load_reg_F: PROCESS
BEGIN
	load_reg_F <= '0';
	WAIT FOR 100000 ps;
	load_reg_F <= '1';
WAIT;
END PROCESS t_prcs_load_reg_F;

-- load_reg_S
t_prcs_load_reg_S: PROCESS
BEGIN
	load_reg_S <= '0';
	WAIT FOR 100000 ps;
	load_reg_S <= '1';
WAIT;
END PROCESS t_prcs_load_reg_S;

-- Sel_D
t_prcs_Sel_D: PROCESS
BEGIN
	Sel_D <= '1';
	WAIT FOR 100000 ps;
	Sel_D <= '0';
WAIT;
END PROCESS t_prcs_Sel_D;

-- set_reg_F
t_prcs_set_reg_F: PROCESS
BEGIN
	set_reg_F <= '1';
	WAIT FOR 100000 ps;
	set_reg_F <= '0';
WAIT;
END PROCESS t_prcs_set_reg_F;

-- shift_reg_R
t_prcs_shift_reg_R: PROCESS
BEGIN
	shift_reg_R <= '0';
	WAIT FOR 100000 ps;
	shift_reg_R <= '1';
WAIT;
END PROCESS t_prcs_shift_reg_R;
END main_processor_param_arch;
