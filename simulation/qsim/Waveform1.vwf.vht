-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- *****************************************************************************
-- This file contains a Vhdl test bench with test vectors .The test vectors     
-- are exported from a vector file in the Quartus Waveform Editor and apply to  
-- the top level entity of the current Quartus project .The user can use this   
-- testbench to simulate his design using a third-party simulation tool .       
-- *****************************************************************************
-- Generated on "07/13/2020 15:31:41"
                                                             
-- Vhdl Test Bench(with test vectors) for design  :          Secuencial
-- 
-- Simulation tool : 3rd Party
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY Secuencial_vhd_vec_tst IS
END Secuencial_vhd_vec_tst;
ARCHITECTURE Secuencial_arch OF Secuencial_vhd_vec_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL CLK : STD_LOGIC;
SIGNAL DATO : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL Root : STD_LOGIC_VECTOR(10 DOWNTO 0);
SIGNAL RST : STD_LOGIC;
SIGNAL STR : STD_LOGIC;
COMPONENT Secuencial
	PORT (
	CLK : IN STD_LOGIC;
	DATO : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	Root : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
	RST : IN STD_LOGIC;
	STR : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : Secuencial
	PORT MAP (
-- list connections between master ports and signals
	CLK => CLK,
	DATO => DATO,
	Root => Root,
	RST => RST,
	STR => STR
	);

-- CLK
t_prcs_CLK: PROCESS
BEGIN
LOOP
	CLK <= '0';
	WAIT FOR 5000 ps;
	CLK <= '1';
	WAIT FOR 5000 ps;
	IF (NOW >= 1000000 ps) THEN WAIT; END IF;
END LOOP;
END PROCESS t_prcs_CLK;

-- RST
t_prcs_RST: PROCESS
BEGIN
	RST <= '0';
WAIT;
END PROCESS t_prcs_RST;

-- STR
t_prcs_STR: PROCESS
BEGIN
	STR <= '0';
	WAIT FOR 10000 ps;
	STR <= '1';
	WAIT FOR 10000 ps;
	STR <= '0';
WAIT;
END PROCESS t_prcs_STR;
-- DATO[31]
t_prcs_DATO_31: PROCESS
BEGIN
	DATO(31) <= '0';
WAIT;
END PROCESS t_prcs_DATO_31;
-- DATO[30]
t_prcs_DATO_30: PROCESS
BEGIN
	DATO(30) <= '0';
WAIT;
END PROCESS t_prcs_DATO_30;
-- DATO[29]
t_prcs_DATO_29: PROCESS
BEGIN
	DATO(29) <= '0';
WAIT;
END PROCESS t_prcs_DATO_29;
-- DATO[28]
t_prcs_DATO_28: PROCESS
BEGIN
	DATO(28) <= '0';
WAIT;
END PROCESS t_prcs_DATO_28;
-- DATO[27]
t_prcs_DATO_27: PROCESS
BEGIN
	DATO(27) <= '0';
WAIT;
END PROCESS t_prcs_DATO_27;
-- DATO[26]
t_prcs_DATO_26: PROCESS
BEGIN
	DATO(26) <= '0';
WAIT;
END PROCESS t_prcs_DATO_26;
-- DATO[25]
t_prcs_DATO_25: PROCESS
BEGIN
	DATO(25) <= '0';
WAIT;
END PROCESS t_prcs_DATO_25;
-- DATO[24]
t_prcs_DATO_24: PROCESS
BEGIN
	DATO(24) <= '0';
WAIT;
END PROCESS t_prcs_DATO_24;
-- DATO[23]
t_prcs_DATO_23: PROCESS
BEGIN
	DATO(23) <= '0';
WAIT;
END PROCESS t_prcs_DATO_23;
-- DATO[22]
t_prcs_DATO_22: PROCESS
BEGIN
	DATO(22) <= '1';
WAIT;
END PROCESS t_prcs_DATO_22;
-- DATO[21]
t_prcs_DATO_21: PROCESS
BEGIN
	DATO(21) <= '0';
WAIT;
END PROCESS t_prcs_DATO_21;
-- DATO[20]
t_prcs_DATO_20: PROCESS
BEGIN
	DATO(20) <= '0';
WAIT;
END PROCESS t_prcs_DATO_20;
-- DATO[19]
t_prcs_DATO_19: PROCESS
BEGIN
	DATO(19) <= '1';
WAIT;
END PROCESS t_prcs_DATO_19;
-- DATO[18]
t_prcs_DATO_18: PROCESS
BEGIN
	DATO(18) <= '0';
WAIT;
END PROCESS t_prcs_DATO_18;
-- DATO[17]
t_prcs_DATO_17: PROCESS
BEGIN
	DATO(17) <= '0';
WAIT;
END PROCESS t_prcs_DATO_17;
-- DATO[16]
t_prcs_DATO_16: PROCESS
BEGIN
	DATO(16) <= '1';
WAIT;
END PROCESS t_prcs_DATO_16;
-- DATO[15]
t_prcs_DATO_15: PROCESS
BEGIN
	DATO(15) <= '0';
WAIT;
END PROCESS t_prcs_DATO_15;
-- DATO[14]
t_prcs_DATO_14: PROCESS
BEGIN
	DATO(14) <= '0';
WAIT;
END PROCESS t_prcs_DATO_14;
-- DATO[13]
t_prcs_DATO_13: PROCESS
BEGIN
	DATO(13) <= '0';
WAIT;
END PROCESS t_prcs_DATO_13;
-- DATO[12]
t_prcs_DATO_12: PROCESS
BEGIN
	DATO(12) <= '1';
WAIT;
END PROCESS t_prcs_DATO_12;
-- DATO[11]
t_prcs_DATO_11: PROCESS
BEGIN
	DATO(11) <= '0';
WAIT;
END PROCESS t_prcs_DATO_11;
-- DATO[10]
t_prcs_DATO_10: PROCESS
BEGIN
	DATO(10) <= '0';
WAIT;
END PROCESS t_prcs_DATO_10;
-- DATO[9]
t_prcs_DATO_9: PROCESS
BEGIN
	DATO(9) <= '0';
WAIT;
END PROCESS t_prcs_DATO_9;
-- DATO[8]
t_prcs_DATO_8: PROCESS
BEGIN
	DATO(8) <= '0';
WAIT;
END PROCESS t_prcs_DATO_8;
-- DATO[7]
t_prcs_DATO_7: PROCESS
BEGIN
	DATO(7) <= '1';
WAIT;
END PROCESS t_prcs_DATO_7;
-- DATO[6]
t_prcs_DATO_6: PROCESS
BEGIN
	DATO(6) <= '0';
WAIT;
END PROCESS t_prcs_DATO_6;
-- DATO[5]
t_prcs_DATO_5: PROCESS
BEGIN
	DATO(5) <= '0';
WAIT;
END PROCESS t_prcs_DATO_5;
-- DATO[4]
t_prcs_DATO_4: PROCESS
BEGIN
	DATO(4) <= '0';
WAIT;
END PROCESS t_prcs_DATO_4;
-- DATO[3]
t_prcs_DATO_3: PROCESS
BEGIN
	DATO(3) <= '0';
WAIT;
END PROCESS t_prcs_DATO_3;
-- DATO[2]
t_prcs_DATO_2: PROCESS
BEGIN
	DATO(2) <= '1';
WAIT;
END PROCESS t_prcs_DATO_2;
-- DATO[1]
t_prcs_DATO_1: PROCESS
BEGIN
	DATO(1) <= '0';
WAIT;
END PROCESS t_prcs_DATO_1;
-- DATO[0]
t_prcs_DATO_0: PROCESS
BEGIN
	DATO(0) <= '1';
WAIT;
END PROCESS t_prcs_DATO_0;
END Secuencial_arch;
