library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor_2 is
	port( 
		CLK																												: in  std_logic;	
		AS, load_reg_D, load_reg_S, load_reg_F, set_reg_F, clr_reg_R, shift_reg_R				   : in  std_logic;
		Dato																												: in  std_logic_vector(31 downto 0);
		Sel_D, Sel_B2, Sel_A																							: in	std_logic;
		OUTPUT																											: out std_logic_vector(10 downto 0);
		--REMAINDER																										: out	std_logic_vector(22 downto 0);
		C																													: out std_logic
	);
end entity main_processor_2;

architecture RTL of main_processor_2 is
	signal Sel_B			:std_logic_vector(1  downto 0);
	signal A1,B1,S1,M1	:std_logic_vector(22 downto 0);
	signal A2,B2,S2		:std_logic_vector(22 downto 0);
	signal CO,EN_F			:std_logic;
	signal Q_R				:std_logic_vector(10 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(32 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(22 downto 0);
	signal INPUT_F, Q_F	:std_logic_vector(22 downto 0);
	signal padding			:std_logic_vector(0  downto 0):=(others=>'0');

	begin
		
	with Sel_D select INPUT_D<=	
		padding&Dato				when '1',
		Q_D(29 downto 0)&"000"	when '0';
	
	with Sel_A select A2<=
		Q_F							when '0',
		Q_F(20 downto 1)&"001"	when '1';
	
	Sel_B<=Sel_B2&CO;
	
	with Sel_B select B2(15 downto 0)<=
		(others=>'0')	when "00",
		"0000"&Q_R&'0'	when "01",
		'0'&Q_R&"0000"	when "10",
		Q_R&"10000"		when "11";
	
	B2(22 downto 16)<=(others=>'0');
	
	with CO select M1<=
		Q_S(19 downto 0)&Q_D(32 downto 30) when '0',
		S1											  when '1';
	
	REG_D: lpm_ff
		generic map(
			lpm_width	=>33
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	

	REG_S: lpm_ff
		generic map(
			lpm_width	=>23
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_R: lpm_shiftreg
		generic map(
			lpm_width => 11
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_R,
			shiftin	=> CO,
			enable	=> shift_reg_R,
			q			=> Q_R
		);

	EN_F <= set_reg_F or load_reg_F;
	
	REG_F: lpm_ff
		generic map(
			lpm_width  =>23,
			lpm_svalue => "1"
		)
		port map(
			clock	=>CLK,
			enable=>EN_F,
			sload =>load_reg_F,
			sset  =>set_reg_F,
			data	=>INPUT_F,
			q		=>Q_F
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A1,
			datab		=>B1,
			add_sub	=>'0',
			result	=>S1,			
			cout		=>CO
		);
	
	ALU2: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A2,
			datab		=>B2,
			add_sub	=>AS,
			result	=>S2		
		);
	
	A1<=Q_S(19 downto 0)&Q_D(32 downto 30);
	B1<=Q_F;
	
	INPUT_S<=M1;
	INPUT_F<=S2;
		
	OUTPUT	 <=	Q_R;
	C			 <= 	not CO;
	--REMAINDER <=	Q_S;
		
end RTL;