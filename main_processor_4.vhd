library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor_4 is
	port( 
		CLK																												: in  std_logic;	
		load_reg_D, load_reg_S, load_reg_F, set_reg_F, clr_reg_R, shift_reg_R					   : in  std_logic;
		Dato																												: in  std_logic_vector(31 downto 0);
		Sel_D																												: in	std_logic;
		OUTPUT																											: out std_logic_vector(10 downto 0);
		--REMAINDER																										: out	std_logic_vector(22 downto 0);
		C																													: out std_logic
	);
end entity main_processor_4;

architecture RTL of main_processor_4 is

	signal A1,B1,S1		:std_logic_vector(22 downto 0);
	signal A2,B2,S2		:std_logic_vector(22 downto 0);
	signal A3,B3,S3		:std_logic_vector(22 downto 0);
	signal A4,B4,S4		:std_logic_vector(22 downto 0);
	signal A5,B5,S5		:std_logic_vector(22 downto 0);
	signal CO,EN_F			:std_logic;
	signal Q_R				:std_logic_vector(10 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(32 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(22 downto 0);
	signal INPUT_F, Q_F	:std_logic_vector(22 downto 0);
	signal pad				:std_logic_vector(9  downto 0):=(others=>'0');

	begin
		
	A1<=Q_S(19 downto 0)&Q_D(32 downto 30);
	B1<=Q_F;
	
	A2<=Q_F(20 downto 1)&"001";
	B2<=pad&Q_R&"10";
	
	A3<=Q_F(20 downto 1)&"001";
	B3<=pad&Q_R&"00";
	
	A4<=S2;
	B4<=pad(6 downto 0)&Q_R&"10000";
	
	A5<=S3;
	B5<=pad(7 downto 0)&Q_R&"0000";
	
	with CO select INPUT_S<=
		Q_S(19 downto 0)&Q_D(32 downto 30) when '0',
		S1											 when '1';
		
	with CO select INPUT_F<=
		S5 when '0',
		S4 when '1';
		
	with Sel_D select INPUT_D<=
		'0'&Dato when '1',
		Q_D(29 downto 0)&"000" when '0';
	
	REG_D: lpm_ff
		generic map(
			lpm_width	=>33
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	

	REG_S: lpm_ff
		generic map(
			lpm_width	=>23
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_R: lpm_shiftreg
		generic map(
			lpm_width => 11
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_R,
			shiftin	=> CO,
			enable	=> shift_reg_R,
			q			=> Q_R
		);

	EN_F <= set_reg_F or load_reg_F;
	
	REG_F: lpm_ff
		generic map(
			lpm_width  =>23,
			lpm_svalue => "1"
		)
		port map(
			clock	=>CLK,
			enable=>EN_F,
			sload =>load_reg_F,
			sset  =>set_reg_F,
			data	=>INPUT_F,
			q		=>Q_F
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A1,
			datab		=>B1,
			add_sub	=>'0',
			result	=>S1,			
			cout		=>CO
		);
	
	ALU2: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A2,
			datab		=>B2,
			result	=>S2		
		);
	
	ALU3: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A3,
			datab		=>B3,
			result	=>S3		
		);
		
	ALU4: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A4,
			datab		=>B4,
			result	=>S4		
		);
	
	ALU5: lpm_add_sub
		generic map(
			lpm_width				=>23,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A5,
			datab		=>B5,
			add_sub  =>'0',
			result	=>S5		
		);
		
	
	OUTPUT	 <=	Q_R;
	C			 <= 	not CO;
	--REMAINDER <=	Q_S;
		
end RTL;