library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor_min_param is
	generic(
		SA1 :positive:=475;--size of adder 1
		SA2 :positive:=345;--size of adder 2 and 3
		SR  :positive:=171;--size of root
		SI	 :positive:=512;--size of input data
		AB  :positive:=1   --adjusting bit
	);
	port( 
		CLK														: in  std_logic;	
		load_reg_D, load_reg_S, load_reg_F			   : in  std_logic;
		set_reg_F, clr_reg_R, shift_reg_R, Sel_D		: in	std_logic;
		Data														: in  std_logic_vector(SI/2-1 downto 0);
		--Dato														: in  std_logic_vector(SI-1 downto 0);
		--REMAIN														: out	std_logic_vector(SA1-1 downto 0);
		--C															: out std_logic;
		OUTPUT													: out std_logic_vector(SR-1 downto 0)
	);
end entity main_processor_min_param;

architecture RTL of main_processor_min_param is
	signal Dato				:std_logic_vector(SI-1 downto 0):=(others=>'0');
	signal A1,B1,S1,M1	:std_logic_vector(SA1-1 downto 0);
	signal A2,B2,S2		:std_logic_vector(SA2-1 downto 0);
	signal A3,B3,S3		:std_logic_vector(SA2-1 downto 0);
	signal CO,EN_F			:std_logic;
	signal Q_R				:std_logic_vector(SR-1 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(SI+AB-1 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(SA1-1 downto 0);
	signal INPUT_F, Q_F	:std_logic_vector(SA2-1 downto 0);
	signal padding			:std_logic_vector(AB-1  downto 0):=(others=>'0');

	begin
	
	Dato(SI/2-1 downto 0)<=Data;
	
	A1<=Q_S(SA1-4 downto 0)&Q_D(SI+AB-1 downto SI+AB-3);
	B1(SA2-1 downto 0)<=Q_F;
	B1(SA1-1 downto SA2)<=(others=>'0');

	A2(SA2-1 downto 0)<=Q_F(SA2-3 downto 1)&"001";
	B2(SR+1 downto 0)<=Q_R&CO&'0';
	B2(SA2-1 downto SR+2)<=(others=>'0');
	
	A3<=S2;
	
	with Sel_D select INPUT_D<=	
		padding&Dato					when '1',
		Q_D(SI+AB-4 downto 0)&"000"	when '0';
	
	with CO select B3(SR+4 downto 0)<=
		'0'&Q_R&"0000"	when '0',
		Q_R&"10000"		when '1';
	
	B3(SA2-1 downto SR+5)<=(others=>'0');
	
	with CO select M1<=
		Q_S(SA1-4 downto 0)&Q_D(SI+AB-1 downto SI+AB-3) when '0',
		S1											  when '1';
	
	REG_D: lpm_ff
		generic map(
			lpm_width	=>SI+AB
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			enable=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	

	REG_S: lpm_ff
		generic map(
			lpm_width	=>SA1
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			enable=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_R: lpm_shiftreg
		generic map(
			lpm_width => SR
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_R,
			shiftin	=> CO,
			enable	=> shift_reg_R,
			q			=> Q_R
		);

	EN_F <= set_reg_F or load_reg_F;
	
	REG_F: lpm_ff
		generic map(
			lpm_width  =>SA2,
			lpm_svalue => "1"
		)
		port map(
			clock	=>CLK,
			enable=>EN_F,
			sload =>load_reg_F,
			sset  =>set_reg_F,
			data	=>INPUT_F,
			q		=>Q_F
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>SA1,
			lpm_direction			=>"SUB",
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A1,
			datab		=>B1,
			result	=>S1,			
			cout		=>CO
		);
	
	ALU2: lpm_add_sub
		generic map(
			lpm_width				=>SA2,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A2,
			datab		=>B2,
			result	=>S2		
		);
	
	ALU3: lpm_add_sub
		generic map(
			lpm_width				=>SA2,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A3,
			datab		=>B3,
			add_sub	=>CO,
			result	=>S3		
		);
	

	INPUT_S<=M1;
	INPUT_F<=S3;
		
	OUTPUT	<=	Q_R;
	--REMAIN	<= Q_S;
	--C			 <= 	not CO;
	--FACTOR <=	Q_F;
		
end RTL;