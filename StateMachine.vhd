-- Quartus Prime VHDL Template
-- Four-State Mealy State Machine

-- A Mealy machine has outputs that depend on both the state and
-- the inputs.	When the inputs change, the outputs are updated
-- immediately, without waiting for a clock edge.  The outputs
-- can be written more than once per state or per clock cycle.

library ieee;
use ieee.std_logic_1164.all;


entity StateMachine is

	port
	(
		clk, start, reset									: in std_logic;
		cnt													: in std_logic_vector(6 downto 0);
		load_reg_D, load_reg_S, load_reg_F			: out std_logic;
		set_reg_F, clr_reg_R, shift_reg_R,Sel_D	: out	std_logic;
		set_count, count									: out std_logic
	);

end entity;

architecture rtl of StateMachine is

	-- Build an enumerated type for the state machine
	type state_type is (s0, s1);

	-- Register to hold the current state
	signal state : state_type;
	
	signal zeros : std_logic_vector(6 downto 0);

begin
	
	zeros <= (others => '0');

	process (clk, start, reset, cnt)
	begin

		if reset = '1' then
			state <= s0;

		elsif (rising_edge(clk)) then

			-- Determine the next state synchronously, based on
			-- the current state and the input
			case state is
				when s0=>
					if start = '1' then
						state <= s1;
					else
						state <= s0;
					end if;
				when s1=>
					if cnt=zeros then
						state <= s0;
					else
						state <= s1;
					end if;
			end case;

		end if;
	end process;

	-- Determine the output based only on the current state
	-- and the input (do not wait for a clock edge).
	process (clk, start, reset, cnt)
	begin
			case state is
				when s0=>
					if start = '1' then
						load_reg_D	<= '1';
						load_reg_S	<= '0';
						load_reg_F	<= '0';
						set_reg_F	<= '1';
						clr_reg_R	<= '1';
						shift_reg_R <= '0';
						Sel_D			<= '1';
						set_count	<= '1';
						count			<= '1';
					else
						load_reg_D	<= '0';
						load_reg_S	<= '0';
						load_reg_F	<= '0';
						set_reg_F	<= '0';
						clr_reg_R	<= '0';
						shift_reg_R <= '0';
						Sel_D			<= '0';
						set_count	<= '0';
						count			<= '0';	
					end if;
				when s1=>
					if cnt=zeros then
						load_reg_D	<= '0';
						load_reg_S	<= '0';
						load_reg_F	<= '0';
						set_reg_F	<= '0';
						clr_reg_R	<= '0';
						shift_reg_R <= '0';
						Sel_D			<= '0';
						set_count	<= '0';
						count			<= '0';
					else
						load_reg_D	<= '1';
						load_reg_S	<= '0';
						load_reg_F	<= '0';
						set_reg_F	<= '1';
						clr_reg_R	<= '1';
						shift_reg_R <= '0';
						Sel_D			<= '1';
						set_count	<= '1';
						count			<= '1';
					end if;
					
			end case;
	end process;

end rtl;
