library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor_5 is
	port( 
		CLK																												: in  std_logic;	
		load_reg_D, load_reg_S, load_reg_W, load_reg_R, clr_reg_Q, shift_reg_Q					   : in  std_logic;
		Dato																												: in  std_logic_vector(32 downto 0);
		Sel_D																												: in	std_logic;
		OUTPUT																											: out std_logic_vector(10 downto 0);
		--REMAINDER																										: out	std_logic_vector(22 downto 0);
		C																													: out std_logic
	);
end entity main_processor_5;

architecture RTL of main_processor_5 is
	signal Sel_B			:std_logic_vector(1  downto 0);
	signal A1,B1,S1		:std_logic_vector(21 downto 0);
	signal A2,B2,S2		:std_logic_vector(24 downto 0);
	signal A3,B3,S3		:std_logic_vector(24 downto 0);
	signal A4,B4,S4		:std_logic_vector(24 downto 0);
	signal A5,B5,S5		:std_logic_vector(24 downto 0);
	signal A6,B6,S6		:std_logic_vector(21 downto 0);
	signal A7,B7,S7		:std_logic_vector(21 downto 0);
	signal A8,B8,S8		:std_logic_vector(21 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(29 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(21 downto 0);
	signal INPUT_R, Q_R	:std_logic_vector(24 downto 0);
	signal INPUT_W, Q_W	:std_logic_vector(24 downto 0);
	signal Q_Q				:std_logic_vector(10 downto 0);
	signal pad				:std_logic_vector(13  downto 0):=(others=>'0');


	begin
	
	
	A1<=Q_S;
	B1<=pad(10 downto 0)&Q_Q;
	
	A2<="000"&Q_S;
	B2<="000"&Q_S;
	
	A3<=S4;
	B3<=S5;
	
	A4<="000"&Q_S;
	B4<=pad&Q_Q;
	
	A5<="000"&Q_S;
	B5<=pad&Q_Q;
	
	A6<=Q_R(21 downto 0);
	B6<=Q_W(21 downto 0);
	
	A7<=S6;
	B7<=S8;
	
	A8<=pad(10 downto 0)&Q_Q;
	B8<=pad(10 downto 0)&Q_Q;
		
	with S7(21) select INPUT_W<=
		S2 when '0',
		S3 when '1';
		
	with S7(21) select INPUT_S<=
		Q_S when '0',
		S1	 when '1';
		
	with S7(21) select INPUT_R(24 downto 3)<=
		Q_R(21 downto 0)	when '0',
		S7  					when '1';
		
	with Sel_D select INPUT_R(2 downto 0)<=
		Q_D(29 downto 27) when '0',
		Dato(32 downto 30) when '1';
		
	with Sel_D select INPUT_D<=
		Q_D(26 downto 0)&"000" when '0',
		Dato(29 downto 0) when '1';
		
	REG_w: lpm_ff
		generic map(
			lpm_width	=>25
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_w,
			data	=>INPUT_w,
			q		=>Q_w
		);	
		
	REG_D: lpm_ff
		generic map(
			lpm_width	=>30
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	


	REG_S: lpm_ff
		generic map(
			lpm_width	=>22
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_Q: lpm_shiftreg
		generic map(
			lpm_width => 11
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_Q,
			shiftin	=> (not S7(21)),
			enable	=> shift_reg_Q,
			q			=> Q_Q
		);

	
	REG_R: lpm_ff
		generic map(
			lpm_width  =>25
		)
		port map(
			clock	=>CLK,
			sload =>load_reg_R,
			data	=>INPUT_R,
			q		=>Q_R
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>22,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A1,
			datab		=>B1,
			result	=>S1			
		);
	
	ALU2: lpm_add_sub
		generic map(
			lpm_width				=>25,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A2,
			datab		=>B2,
			result	=>S2		
		);
	
	ALU3: lpm_add_sub
		generic map(
			lpm_width				=>25,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A3,
			datab		=>B3,
			result	=>S3		
		);
		
	ALU4: lpm_add_sub
		generic map(
			lpm_width				=>25,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A4,
			datab		=>B4,
			result	=>S4		
		);	
	
	ALU5: lpm_add_sub
		generic map(
			lpm_width				=>25,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A5,
			datab		=>B5,
			result	=>S5		
		);
	
	ALU6: lpm_add_sub
		generic map(
			lpm_width				=>22,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A6,
			datab		=>B6,
			result	=>S6		
		);
	
	ALU7: lpm_add_sub
		generic map(
			lpm_width				=>22,
			lpm_representation	=>"unsigned",
			lpm_direction			=>"SUB"
		)
		port map(
			dataa		=>A7,
			datab		=>B7,
			result	=>S7		
		);
	
	ALU8: lpm_add_sub
		generic map(
			lpm_width				=>22,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A8,
			datab		=>B8,
			result	=>S8		
		);	

	OUTPUT	 <=	Q_Q;
	C			 <= 	not S7(21);
	--REMAINDER <=	Q_S;
		
end RTL;