library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY lpm;
USE lpm.lpm_components.all;

entity main_processor is
	generic(
		SA  :positive:=89;--size of adder 1
		SR  :positive:=43;--size of root
		SI	 :positive:=128;--size of input data
		AB  :positive:=1   --adjusting bit
	);
	port( 
		CLK																												: in  std_logic;	
		AS, load_reg_D, load_reg_S, load_reg_F, set_reg_F, clr_reg_R, shift_reg_R				   : in  std_logic;
		--Dato																												: in  std_logic_vector(SI-1 downto 0);
		data																												: in std_logic_vector(SI/2-1 downto 0);
		Sel_D, Sel_B2																									: in	std_logic;
		Sel_A, Sel_B																									: in  std_logic_vector(1  downto 0);
		OUTPUT																											: out std_logic_vector(SR-1 downto 0);
		--REMAINDER																										: out	std_logic_vector(SA-1 downto 0);
		C																													: out std_logic
	);
end entity main_processor;

architecture RTL of main_processor is
	signal Dato				:std_logic_vector(SI-1 downto 0):=(others=>'0');
	signal A,B,S			:std_logic_vector(SA-1 downto 0);
	signal CO,EN_F			:std_logic;
	signal Q_R				:std_logic_vector(SR-1 downto 0);
	signal INPUT_D, Q_D	:std_logic_vector(SI+AB-1 downto 0);
	signal INPUT_s, Q_S	:std_logic_vector(SA-1 downto 0);
	signal INPUT_F, Q_F	:std_logic_vector(SA-1 downto 0);
	signal padding			:std_logic_vector(AB-1  downto 0):=(others=>'0');

	begin
		
	Dato(SI/2-1 downto 0)<=data;	
		
	with Sel_D select INPUT_D<=	
		padding&Dato				when '1',
		Q_D(SI+AB-4 downto 0)&"000"	when '0';
	
	with Sel_A select A<=
		Q_S(SA-4 downto 0)&Q_D(SI+AB-1 downto SI+AB-3)	when "00",
		Q_F															when "01",
		Q_F(SA-3 downto 1)&"001"									when "10",
		(others=>'Z')												when "11";
		
	with Sel_B select B(SR+3 downto 0)<=
		Q_F(SR+3 downto 0)	when "00",
		"000"&Q_R&'0'		when "01",
		'0'&Q_R&"000"		when "10",
		Q_R&"0000"			when "11";
	
	with Sel_B2 select B(SA-1 downto SR+4)<=
		Q_F(SA-1 downto SR+4) when '0',
		(others=>'0')		when '1';
	
	REG_D: lpm_ff
		generic map(
			lpm_width	=>SI+AB
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_D,
			data	=>INPUT_D,
			q		=>Q_D
		);	

	REG_S: lpm_ff
		generic map(
			lpm_width	=>SA
		)
		port map(
			clock	=>CLK,
			sload	=>load_reg_S,
			data	=>INPUT_S,
			q		=>Q_S	
		);
		
	REG_R: lpm_shiftreg
		generic map(
			lpm_width => SR
		)
		port map(
			clock 	=> CLK,
			sclr 		=> clr_reg_R,
			shiftin	=> CO,
			enable	=> shift_reg_R,
			q			=> Q_R
		);

	EN_F <= set_reg_F or load_reg_F;
	
	REG_F: lpm_ff
		generic map(
			lpm_width  =>SA,
			lpm_svalue => "1"
		)
		port map(
			clock	=>CLK,
			enable=>EN_F,
			sload =>load_reg_F,
			sset  =>set_reg_F,
			data	=>INPUT_F,
			q		=>Q_F
		);

	ALU: lpm_add_sub
		generic map(
			lpm_width				=>SA,
			lpm_representation	=>"unsigned"		
		)
		port map(
			dataa		=>A,
			datab		=>B,
			add_sub	=>AS,
			result	=>S,			
			cout		=>CO
		);
		
	INPUT_S<=S;
	INPUT_F<=S;
		
	OUTPUT	 <=	Q_R;
	C			 <= 	not CO;
	--REMAINDER <=	Q_S;
		
end RTL;